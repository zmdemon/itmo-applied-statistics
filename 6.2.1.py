import numpy
import statistics
import decimal

sample = [int(input('Введите ' + str(i + 1) + '-ое значение выборки: ')) for i in range(int(input('Введите объем выборки (количество алхимиков): ')))]
sample.sort()
table = [[], []]
for i in range(1, 7):
	if sample.count(i) > 0:
		table[0].append(i)
		table[1].append(sample.count(i) / len(sample))
		print('P(\u03B5* = ' + str(i) + ') = ' + str(sample.count(i)) + '/' + str(len(sample)))
	else:
		print('P(\u03B5* = ' + str(i) + ') = 0')
E = numpy.average(table[0], weights = table[1])
D = numpy.average([(i - E) ** 2 for i in table[0]], weights = table[1])
print('Математическое ожидание: ' + str(decimal.Decimal(E).quantize(decimal.Decimal('0.01'))))
print('Дисперсия: ' + str(decimal.Decimal(D).quantize(decimal.Decimal('0.01'))))
print('Среднеквадратическое отклонение: ' + str(decimal.Decimal(D ** 0.5).quantize(decimal.Decimal('0.01'))))
print('Медиана: ' + str(decimal.Decimal(statistics.median(sample)).quantize(decimal.Decimal('1'))))
print('Мода: ' + str(statistics.mode(sample)))